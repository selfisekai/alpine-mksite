---
title: 'Alpine 3.10.6, 3.11.8 and 3.12.4 released'
date: 2021-02-23
---

Alpine 3.10.6, 3.11.8 and 3.12.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.10.6, 3.11.8 and 3.12.4 of its Alpine
Linux operating system.

Those releases include an important security fixes for [openssl](https://www.openssl.org/news/secadv/20210216.txt):

- CVE-2021-23841
- CVE-2021-23840
- CVE-2021-23839


