---
title: 'Alpine 3.13.2 released'
date: 2021-02-17
---

Alpine Linux 3.13.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.13.2 of its Alpine Linux operating system.

This release includes a security update for [openssl](https://www.openssl.org/news/secadv/20210216.txt).

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.13.2).

Git Shortlog
------------

<pre>
6543 (1):
      community/gitea: upgrade to 1.13.2

Andy Postnikov (8):
      community/php7-pecl-redis: upgrade to 5.3.3
      community/php8-pecl-redis: upgrade to 5.3.3
      community/php7: security upgrade to 7.4.15 - CVE-2021-21702
      community/php8: security upgrade to 8.0.2 - CVE-2021-21702
      community/php8-pecl-apcu: add patch to fix ui
      community/unit: upgrade to 1.22.0
      community/php7-pecl-maxminddb: upgrade to 1.10.0
      community/php8-pecl-maxminddb: upgrade to 1.10.0

Duncan Bellamy (1):
      community/dovecot-fts-xapian: upgrade to 1.4.7

Francesco Colista (1):
      community/seahorse: upgrade to 3.38.1

Henrik Riomar (1):
      community/vault: security upgrade to 1.5.7

Holger Jaekel (1):
      community/postgis: upgrade to 3.1.1

J0WI (4):
      community/mozjs78: security upgrade to 78.7.0
      community/firefox-esr: security upgrade to 78.7.0
      community/firefox: security upgrade to 85.0.2
      community/firefox-esr: upgrade to 78.7.1

Jake Buchholz (2):
      community/docker: security upgrade to 20.10.3
      community/runc: security upgrade to 1.0.0_rc93

Jakub Jirutka (9):
      community/muacme: upgrade to 0.4.0
      community/postgresql-tsearch-czech: fix error in create script
      community/ruby-timecop: upgrade to 0.9.3
      community/ruby-i18n: upgrade to 1.8.8
      community/ruby-rspec-mocks: upgrade to 3.10.2
      community/ruby-rspec-support: upgrade to 3.10.2
      community/postgresql-plpgsql_check: upgrade to 1.15.2
      main/knot: upgrade to 3.0.4
      community/ruby-ox: upgrade to 2.14.1

Jellyfrog (1):
      main/net-snmp: enable aes256 support

Kevin Daudt (1):
      main/screen: use better patch for CVE-2021-26937

Leo (42):
      community/librsvg: upgrade to 2.50.3
      main/e2fsprogs: upgrade to 1.45.7
      main/perl-specio: upgrade to 0.47
      community/wireshark: security upgrade to 3.4.3
      community/entr: upgrade to 4.7
      main/py3-packaging: upgrade to 20.9
      main/ansible-base: upgrade to 2.10.5
      main/privoxy: upgrade to 3.0.31
      community/mousepad: upgrade to 0.5.2
      community/libnumbertext: upgrade to 1.0.7
      community/i3wm: upgrade to 4.19.1
      community/evince: upgrade to 3.38.1
      community/py3-bleach: fix GHSA-vv2x-vrpj-qqpq
      main/perl-test-output: upgrade to 1.032
      community/mumble: fix murmur-openrc function
      community/py3-tox: upgrade to 3.21.4
      community/tor: upgrade to 0.4.4.7
      community/tor: downgrade to 0.4.4.6
      community/tor: upgrade to 0.4.4.7
      community/tor: fix checksum
      community/py3-natsort: upgrade to 7.1.1
      testing/i3wm-gaps: upgrade to 4.19.1
      community/gpaste: upgrade to 3.38.5
      community/gnome-sound-recorder: upgrade to 3.38.1
      community/gnome-builder: upgrade to 3.38.2
      community/qca: upgrade to 2.3.2
      community/collectd: fix linking with python3.8
      community/connman: fix CVE-2021-26676
      main/wpa_supplicant: fix CVE-2021-0326
      community/pipewire: upgrade to 0.3.21
      community/py3-prompt_toolkit: upgrade to 3.0.15
      main/glib: upgrade to 2.66.6
      main/subversion: security upgrade to 1.14.1
      community/openvswitch: security upgrade to 2.12.3
      community/py3-prompt_toolkit: upgrade to 3.0.16
      main/glib: upgrade to 2.66.7
      main/screen: fix CVE-2021-26937
      main/pango: upgrade to 1.48.2
      main/audit: enable support for arm and aarch64
      main/libusb: fix handling buggy USB devices
      community/libqmi: upgrade to 1.26.10
      main/openssl: upgrade to 1.1.1j

Michał Polański (12):
      main/fuse3: upgrade to 3.10.2
      main/py3-iso8601: upgrade to 0.1.14
      community/btrfs-compsize: upgrade to 1.4
      main/py3-cryptography: security upgrade to 3.3.2
      main/clamav: upgrade to 0.103.1
      community/tllist: upgrade to 1.0.5
      community/httpie: upgrade to 2.4.0
      community/wlr-randr: upgrade to 0.2.0
      community/ginkgo: upgrade to 1.15.0
      main/py3-more-itertools: upgrade to 8.7.0
      community/btrfs-compsize: upgrade to 1.5
      main/git: upgrade to 2.30.1

Milan P. Stanić (6):
      community/iotop-c: upgrade to 1.17
      main/haproxy: upgrade to 2.2.9
      community/perl-crypt-openssl-x509: fix depends
      community/libudev-zero: upgrade to 0.4.8
      community/exfatprogs: upgrade to 1.1.0
      main/busybox: add few serial devices to /etc/securetty

Natanael Copa (39):
      main/linux-lts: backport fix for xen domU
      community/jool-modules-lts: rebuild against kernel 5.10.11-r1
      community/rtl8821ce-lts: rebuild against kernel 5.10.11-r1
      community/rtpengine-lts: rebuild against kernel 5.10.11-r1
      main/dahdi-linux-lts: rebuild against kernel 5.10.11-r1
      main/xtables-addons-lts: rebuild against kernel 5.10.11-r1
      main/zfs-lts: rebuild against kernel 5.10.11-r1
      main/linux-lts: enable VF driver for Intel X700/E800 NICs
      main/linux-rpi: upgrade to 5.10.12
      community/jool-modules-rpi: rebuild against kernel 5.10.12-r0
      main/zfs-rpi: rebuild against kernel 5.10.12-r0
      main/linux-lts: upgrade to 5.10.12
      community/jool-modules-lts: rebuild against kernel 5.10.12-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.12-r0
      community/rtpengine-lts: rebuild against kernel 5.10.12-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.12-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.12-r0
      main/zfs-lts: rebuild against kernel 5.10.12-r0
      main/linux-lts: upgrade to 5.10.15
      community/jool-modules-lts: rebuild against kernel 5.10.15-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.15-r0
      community/rtpengine-lts: rebuild against kernel 5.10.15-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.15-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.15-r0
      main/zfs-lts: rebuild against kernel 5.10.15-r0
      main/linux-rpi: upgrade to 5.10.15
      community/jool-modules-rpi: rebuild against kernel 5.10.15-r0
      main/zfs-rpi: rebuild against kernel 5.10.15-r0
      main/linux-rpi: upgrade to 5.10.16
      community/jool-modules-rpi: rebuild against kernel 5.10.16-r0
      main/zfs-rpi: rebuild against kernel 5.10.16-r0
      main/linux-lts: upgrade to 5.10.16
      community/jool-modules-lts: rebuild against kernel 5.10.16-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.16-r0
      community/rtpengine-lts: rebuild against kernel 5.10.16-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.16-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.16-r0
      main/zfs-lts: rebuild against kernel 5.10.16-r0
      ===== release 3.13.2 =====

Rasmus Thomsen (3):
      main/glib: add missing secfixes for 2.66.6
      community/firefox: disable smoosh, fails to build
      community/webkit2gtk: upgrade to 2.30.5

Reed Wade (1):
      community/kiln: new aport

Simon Frankenberger (1):
      community/openjdk8: upgrade to 3.17.1 (8.275.01)

TBK (4):
      community/kiln: disable on mips64
      community/mumble: install murmur-openrc if murmur and openrc is installed
      main/libmaxminddb: remove openrc subpackage
      main/openldap: security upgrade to 2.4.57
</pre>
