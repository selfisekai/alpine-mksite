---
title: 'Alpine 3.16.3 released'
date: 2022-11-11
---

Alpine Linux 3.16.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.16.3 of its Alpine Linux operating system.

This is a bugfix release.

The full lists of changes can be found in the [git log](http://git.alpinelinux.org/aports/log/?h=v3.16.3).

Git Shortlog
------------

<pre>
6543 (1):
      community/gitea: upgrade to 1.17.3

Andy Postnikov (14):
      main/postgresql13: security upgrade to 13.8 - CVE-2022-2625
      main/postgresql14: security upgrade to 14.5 - CVE-2022-2625
      community/postgresql12: security upgrade to 12.12 - CVE-2022-2625
      community/phpldapadmin: upgrade to 1.2.6.4
      community/composer: upgrade to 2.4.1
      community/drupal7: upgrade to 7.92
      community/composer: upgrade to 2.4.2
      community/php8: security upgrade to 8.0.24
      community/php81: security upgrade to 8.1.11
      community/php8: security upgrade to 8.0.25
      community/php81: security upgrade to 8.1.12
      community/php8-pecl-xhprof: upgrade to 2.3.8
      community/php81-pecl-xhprof: upgrade to 2.3.8
      community/konsole: remove stale patch from checksums

Anjandev Momi (1):
      community/sxmo-utils: upgrade to 1.9.1

Antoine Martin (10):
      community/dotnet6-runtime: security upgrade to 6.0.8
      community/dotnet6-build: security upgrade to 6.0.108
      community/dotnet6-build: fix RID bug
      community/dotnet6-runtime: fix RID bug
      community/dotnet6-build: upgrade to 6.0.109
      community/dotnet6-runtie: upgrade to 6.0.9
      community/dotnet6-build: security upgrade to 6.0.110
      community/dotnet6-runtime: security upgrade to 6.0.10
      community/dotnet6-build: upgrade to 6.0.111
      community/dotnet6-runtime: upgrade to 6.0.11

Ariadne Conill (1):
      main/alpine-baselayout: restore nsswitch.conf

Bart Ribbers (3):
      community/kopeninghours: new aport
      community/kosmindoormap: depend on kopeninghours
      community/kde-release-service: upgrade to 22.04.3

Clayton Craft (1):
      community/superd: upgrade to 0.6

Dmitry Zakharchenko (2):
      community/py3-tzdata: new aport
      community/py3-pendulum: new aport

Duncan Bellamy (2):
      community/ceph: add py3-ceph-common sub package with core ceph module
      community/rspamd: upgrade to 3.3

Galen Abell (1):
      community/riot-web: security upgrade to 1.11.4

Guillaume Quintard (2):
      main/varnish: upgrade to 7.1.1
      main/varnish: upgrade to 7.1.2

Henrik Riomar (2):
      main/intel-ucode: security upgrade to 20220809
      community/vault: upgrade to 1.10.6

Holger Jaekel (2):
      community/gdal: upgrade to 3.5.3
      community/geos: upgrade to 3.10.3

J0WI (23):
      community/java-postgresql-jdbc: security upgrade to 42.4.2
      main/mariadb: security upgrade to 10.6.9
      community/fdkaac: security upgrade to 1.0.3
      main/dhcp: security upgrade to 4.4.3_p1
      community/imagemagick6: security upgrade to 6.9.12.65
      community/imagemagick: upgrade to 7.1.0.50
      community/vault: security upgrade to 1.10.7
      main/git: security upgrade to 2.36.3
      main/mariadb: upgrade to 10.6.10
      community/py3-django: security upgrade to 3.2.16
      main/nginx: security upgrade to 1.22.1
      community/geth: security upgrade to 1.10.22
      main/darkhttpd: add secfixes
      community/e2guardian: security upgrade to 5.4.5r
      community/libreoffice: patch CVE-2022-3140
      community/erlang: security upgrade to 24.3.4.6
      main/py3-mako: add secfixes
      community/libmodbus: security upgrade to 3.1.8
      community/faad2: security upgrade to 2.10.1
      community/py3-waitress: add secfixes
      community/py3-bottle: security upgrade to 0.12.21
      main/py3-tz: upgrade to 2022.2.1
      main/py3-tz: upgrade to 2022.6

Jake Buchholz Göktürk (5):
      community/runc: upgrade to 1.1.4
      community/containerd: upgrade to 1.6.8
      community/docker; security upgrade to 20.10.18
      community/docker: security upgrade to 20.10.20
      community/docker-cli-compose: security upgrade to 2.12.2

Jakub Jirutka (15):
      main/ruby-bundler: fix --help when man is avail but -doc isn't installed
      main/alpine-make-rootfs: upgrade to 0.6.1
      community/open-vm-tools: security upgrade to 12.1.0
      main/luajit: build with -O2 instead of -Os
      community/docker-registry: add missing depend() to init script
      community/knot-resolver: fix running w/ supervise-daemon, add provide dns
      community/stunnel: disable test p02_require_cert on s390x (hangs)
      community/alpine-make-vm-image: upgrade to 0.10.0
      main/knot: upgrade to 3.1.9
      main/nodejs: fix secfixes
      community/linux-edge: enable CONFIG_NFT_OBJREF
      main/linux-lts: enable missing nftables options
      main/openldap: unify changes and patches for slapd.conf and slapd.ldif
      main/openldap: fix problem with pidfile - service status crashed
      main/openldap: fix invalid includes in slapd.conf

Jordan Christiansen (1):
      community/acme-client: upgrade to 1.3.1

Kaarle Ritvanen (4):
      community/zoneminder: fix various issues
      community/zoneminder: fix init script
      main/logrotate: subpackage for main syslog file
      main/awall: upgrade to 1.12.1

Kevin Daudt (3):
      community/zabbix: explicitly set PluginSocket for agent2
      community/zabbix: include default plugin conf
      community/zabbix: upgrade to 6.0.8

Konstantin Kulikov (4):
      community/grafana-frontend: upgrade to 8.5.11
      community/grafana: upgrade to 8.5.11
      community/grafana-frontend: upgrade to 8.5.13
      community/grafana: security upgrade to 8.5.13

Lauren N. Liberda (1):
      community/riot-web: security upgrade to 1.11.8

Leonardo Arena (4):
      community/nextcloud: upgrade to 24.0.5
      community/nextcloud23: upgrade to 23.0.9
      community/zabbix: upgrade to 6.0.9
      main/protobuf-c: upgrade to 1.4.1

Marc Hassan (1):
      main/nodejs: upgrade to 16.17.1

Michał Polański (2):
      community/buildah: upgrade to 1.26.4
      community/buildah: upgrade to 1.26.5

Milan P. Stanić (25):
      community/linux-edge: upgrade to 5.19.1
      community/linux-edge: remove edge4virt flavor, use linux-edge in VMs
      main/haproxy: upgrade to 2.4.18
      community/linux-edge: upgrade to 5.19.2
      community/linux-edge: upgrade to 5.19.3
      community/linux-edge: upgrade to 5.19.4
      community/linux-edge: fix openssl in makedepends
      community/linux-edge: upgrade to 5.19.6
      community/linux-edge: upgrade to 5.19.7
      community/linux-edge: upgrade to 5.19.8
      community/linux-edge: upgrade to 5.19.9
      community/linux-edge: remove provides/replaces linux-edeg4virt
      community/linux-edge: upgrade to 5.19.10
      community/linux-edge: upgrade to 5.19.11
      community/linux-edge: upgrade to 5.19.12
      community/linux-edge: upgrade to 6.0.0
      community/linux-edge: add speakup patch
      community/linux-edge: upgrade to 6.0.1
      community/linux-edge: upgrade to 6.0.2
      community/linux-edge: upgrade to 6.0.3
      community/linux-edge: upgrade to 6.0.5
      community/linux-edge: upgrade to 6.0.6
      community/linux-edge: fix build on x86_64
      community/linux-edge: upgrade to 6.0.7
      community/linux-edge: upgrade to 6.0.8

Natanael Copa (138):
      main/linux-lts: upgrade to 5.15.60
      community/jool-modules-lts: rebuild against kernel 5.15.60-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.60-r0
      community/rtpengine-lts: rebuild against kernel 5.15.60-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.60-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.60-r0
      main/zfs-lts: rebuild against kernel 5.15.60-r0
      main/linux-lts: upgrade to 5.15.61
      community/jool-modules-lts: rebuild against kernel 5.15.61-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.61-r0
      community/rtpengine-lts: rebuild against kernel 5.15.61-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.61-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.61-r0
      main/zfs-lts: rebuild against kernel 5.15.61-r0
      main/linux-lts: upgrade to 5.15.62
      community/jool-modules-lts: rebuild against kernel 5.15.62-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.62-r0
      community/rtpengine-lts: rebuild against kernel 5.15.62-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.62-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.62-r0
      main/zfs-lts: rebuild against kernel 5.15.62-r0
      main/linux-lts: backport patch for xen issue
      community/jool-modules-lts: rebuild against kernel 5.15.62-r1
      community/rtl8821ce-lts: rebuild against kernel 5.15.62-r1
      community/rtpengine-lts: rebuild against kernel 5.15.62-r1
      main/dahdi-linux-lts: rebuild against kernel 5.15.62-r1
      main/xtables-addons-lts: rebuild against kernel 5.15.62-r1
      main/zfs-lts: rebuild against kernel 5.15.62-r1
      main/linux-lts: upgrade to 5.15.63
      community/jool-modules-lts: rebuild against kernel 5.15.63-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.63-r0
      community/rtpengine-lts: rebuild against kernel 5.15.63-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.63-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.63-r0
      main/zfs-lts: rebuild against kernel 5.15.63-r0
      main/linux-lts: upgrade to 5.15.64
      community/jool-modules-lts: rebuild against kernel 5.15.64-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.64-r0
      community/rtpengine-lts: rebuild against kernel 5.15.64-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.64-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.64-r0
      main/zfs-lts: rebuild against kernel 5.15.64-r0
      main/linux-lts: upgrade to 5.15.67
      community/jool-modules-lts: rebuild against kernel 5.15.67-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.67-r0
      community/rtpengine-lts: rebuild against kernel 5.15.67-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.67-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.67-r0
      main/zfs-lts: rebuild against kernel 5.15.67-r0
      main/linux-lts: upgrade to 5.15.68
      community/jool-modules-lts: rebuild against kernel 5.15.68-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.68-r0
      community/rtpengine-lts: rebuild against kernel 5.15.68-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.68-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.68-r0
      main/zfs-lts: rebuild against kernel 5.15.68-r0
      main/openldap: improve default slapd.ldif
      main/linux-lts: upgrade to 5.15.69
      community/jool-modules-lts: rebuild against kernel 5.15.69-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.69-r0
      community/rtpengine-lts: rebuild against kernel 5.15.69-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.69-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.69-r0
      main/zfs-lts: rebuild against kernel 5.15.69-r0
      main/bind: security upgrade to 9.16.33
      main/linux-lts: upgrade to 5.15.70
      community/jool-modules-lts: rebuild against kernel 5.15.70-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.70-r0
      community/rtpengine-lts: rebuild against kernel 5.15.70-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.70-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.70-r0
      main/zfs-lts: rebuild against kernel 5.15.70-r0
      main/linux-lts: upgrade to 5.15.71
      community/jool-modules-lts: rebuild against kernel 5.15.71-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.71-r0
      community/rtpengine-lts: rebuild against kernel 5.15.71-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.71-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.71-r0
      main/zfs-lts: rebuild against kernel 5.15.71-r0
      main/linux-lts: upgrade to 5.15.72
      community/jool-modules-lts: rebuild against kernel 5.15.72-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.72-r0
      community/rtpengine-lts: rebuild against kernel 5.15.72-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.72-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.72-r0
      main/zfs-lts: rebuild against kernel 5.15.72-r0
      main/linux-lts: upgrade to 5.15.73
      community/jool-modules-lts: rebuild against kernel 5.15.73-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.73-r0
      community/rtpengine-lts: rebuild against kernel 5.15.73-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.73-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.73-r0
      main/zfs-lts: rebuild against kernel 5.15.73-r0
      main/mqtt-exec: upgrade to 0.5
      main/lua-mqtt-publish: upgrade to 0.4
      main/aports-build: fix build error reporting
      main/linux-lts: enable transparent hugepages for virt x86_64
      main/linux-lts: upgrade to 5.15.75
      community/jool-modules-lts: rebuild against kernel 5.15.75-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.75-r0
      community/rtpengine-lts: rebuild against kernel 5.15.75-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.75-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.75-r0
      main/zfs-lts: rebuild against kernel 5.15.75-r0
      main/linux-lts: upgrade to 5.15.76
      community/jool-modules-lts: rebuild against kernel 5.15.76-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.76-r0
      community/rtpengine-lts: rebuild against kernel 5.15.76-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.76-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.76-r0
      main/zfs-lts: rebuild against kernel 5.15.76-r0
      main/linux-rpi: upgrade to 5.15.76
      community/jool-modules-rpi: rebuild against kernel 5.15.76-r0
      main/zfs-rpi: rebuild against kernel 5.15.76-r0
      main/openssl3: upgrade to 3.0.7 (CVE-2022-3602, CVE-2022-3786)
      main/openssl: upgrade to 1.1.1s
      community/sudo: backport fix for CVE-2022-43995
      main/linux-lts: enable CONFIG_IP_VS_MH for virt x86*
      main/linux-lts: upgrade to 5.15.77
      community/jool-modules-lts: rebuild against kernel 5.15.77-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.77-r0
      community/rtpengine-lts: rebuild against kernel 5.15.77-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.77-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.77-r0
      main/zfs-lts: rebuild against kernel 5.15.77-r0
      main/alpine-conf: backport fix for /boot partition size
      main/linux-lts: enable framebuffer console rotation
      main/linux-lts: upgrade to 5.15.78
      community/jool-modules-lts: rebuild against kernel 5.15.78-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.78-r0
      community/rtpengine-lts: rebuild against kernel 5.15.78-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.78-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.78-r0
      main/zfs-lts: rebuild against kernel 5.15.78-r0
      main/linux-rpi: upgrade to 5.15.78
      community/jool-modules-rpi: rebuild against kernel 5.15.78-r0
      main/zfs-rpi: rebuild against kernel 5.15.78-r0
      ===== release 3.16.3 =====

Newbyte (1):
      community/karlender: upgrade to 0.6.1

NickBug (1):
      community/consul-template fix typo error

Nicolas Lorin (1):
      community/ruby-addressable: upgrade to 2.8.1

Oliver Smith (5):
      community/pmbootstrap: upgrade to 1.46.0
      community/pmbootstrap: upgrade to 1.47.0
      community/pmbootstrap: upgrade to 1.47.1
      community/pmbootstrap: upgrade to 1.48.0
      community/pmbootstrap: upgrade to 1.49.0

Pablo Correa Gómez (25):
      community/eog: upgrade to 42.3
      community/evince: upgrade to 42.3
      community/epiphany: upgrade to 42.4
      community/gnome-calculator: upgrade to 42.2
      community/gnome-calendar: upgrade to 42.2
      community/gnome-console: upgrade to 42.2
      community/gnome-shell-extensions: upgrade to 42.3
      community/nautilus: upgrade to 42.2
      community/simple-scan: upgrade to 42.1
      community/gnome-bluetooth: upgrade to 42.2
      community/gnome-desktop: upgrade to 42.4
      community/gnome-initial-setup: upgrade to 42.2
      community/gnome-remote-desktop: upgrade to 42.4
      community/gnome-shell: upgrade to 42.4
      community/mutter: upgrade to 42.4
      community/xdg-desktop-portal-gnome: upgrade to 42.3
      community/gnome-bluetooth: upgrade to 42.4
      community/gnome-control-center: upgrade to 42.4
      community/simple-scan: upgrade to 42.5
      community/yelp: upgrade to 42.2
      community/gnome-desktop: upgrade to 42.5
      community/gnome-remote-desktop: upgrade to 42.5
      community/gnome-shell: upgrade to 42.5
      community/mutter: upgrade to 42.5
      community/yelp-xsl: upgrade to 42.1

Peter Shkenev (1):
      community/virtualbox-guest-additions: security upgrade to 6.1.36

Peter van Dijk (1):
      community/pdns-recursor: upgrade to 4.6.3

Robert Scheck (1):
      main/rsync: upgrade to 3.2.7

Sean McAvoy (1):
      community/jenkins: security upgrade to 2.346.2

Simon Frankenberger (6):
      community/openjdk13: upgrade to 13.0.12
      community/openjdk11: upgrade to 11.0.16.1
      community/openjdk17: upgrade to 17.0.4.1
      community/openjdk17: upgrade to 17.0.5
      community/openjdk11: upgrade to 11.0.17
      community/openjdk11: backport JDK-8267908, fix s390x again

Steve McMaster (1):
      community/suricata: upgrade to 6.0.8

Síle Ekaterin Liszka (1):
      community/nheko: fix CVE-2022-39264

Sören Tempel (2):
      community/go: upgrade to 1.18.7
      main/mosquitto: backport upstream patch for SIGPIPE issue

Tim Stanley (1):
      community/ebusd: upgrade to 22.4

Timo Teräs (2):
      community/openjdk8: security upgrade to 3.24.0
      main/shared-mime-info: remove sync call from trigger

donoban (1):
      community/xorgxrdp: enable x86

macmpi (1):
      community/acpid: upgrade to 2.0.34

omni (22):
      community/tor: upgrade to 0.4.7.10
      community/qutebrowser: depend on py3-pygments
      community/qt5-qtwebengine: chromium security upgrade
      community/dendrite: security upgrade to 0.9.8
      main/expat: security upgrade to 2.4.9
      community/knot-resolver: security upgrade to 5.5.3
      community/consul: security upgrade to 1.12.5
      main/strongswan: add mitigations for CVE-2022-40617
      community/py3-gpep517: backport from edge
      community/pgcli: add missing dependency on py3-pendulum
      community/pgcli: add additional dependencies
      main/linux-lts: upgrade to 5.15.74
      main/dahdi-linux-lts: rebuild against kernel 5.15.74-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.74-r0
      main/zfs-lts: rebuild against kernel 5.15.74-r0
      community/jool-modules-lts: rebuild against kernel 5.15.74-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.74-r0
      community/rtpengine-lts: rebuild against kernel 5.15.74-r0
      main/xen: add mitigations for XSA-412 & XSA-414
      community/qt5-qtwebengine: chromium security upgrade
      community/ansible-core: upgrade to 2.13.6
      main/xen: upgrade to 4.16.2 & add mitigations for XSA-422

psykose (95):
      community/qt5-qtdeclarative: reduce dbg size
      community/qt6-qtbase: reduce dbg size
      community/qt5-qtbase: reduce dbg size
      community/kwin: reduce dbg size
      main/mesa: reduce dbg size
      community/qt5-qtbase: ..but without deleting the commit
      main/libxml2: update secfixes
      main/sqlite: update secfixes
      community/chromium: upgrade to 102.0.5005.173
      main/tzdata: upgrade to 2022b
      main/tzdata: upgrade to 2022c
      main/libxml2: fix CVE-2022-3209
      community/firefox-esr: upgrade to 91.13.0
      community/thunderbird: upgrade to 91.13.0
      main/luajit: remove -msse4.2
      main/mariadb: disable test-aes
      main/mariadb: ..with the correct name
      community/glib-networking: upgrade to 2.72.1
      community/webkit2gtk: upgrade to 2.36.7
      community/webkit2gtk-5.0: upgrade to 2.36.7
      community/vectorscan: reduce -march level
      community/libreswan: fix runscript
      main/curl: patch CVE-2022-32252
      main/curl: correct secfix typo
      main/ssmtp: fix doc install location
      community/go: upgrade to 1.18.6
      testing/*: rebuild with go 1.18.6
      community/*: rebuild with go 1.18.6
      community/go: update secfixes
      community/ginkgo: disable check on s390x
      community/stunnel: fix sh syntax
      main/redis: upgrade to 7.0.5
      community/nodejs-current: upgrade to 18.9.1
      main/rsync: upgrade to 3.2.5
      community/chromium: upgrade to 102.0.5005.182
      community/grpc-java: actually apply the patch
      main/tiff: upgrade to 4.4.0
      community/php81: disable tests for armv7
      main/libxml2: fix secfix typo
      main/dbus: upgrade to 1.14.4
      community/*: rebuild against go 1.18.7
      community/imagemagick: upgrade to 7.1.0.36
      community/imagemagick: upgrade to 7.1.0.37
      community/imagemagick: upgrade to 7.1.0.38
      community/imagemagick: upgrade to 7.1.0.39
      community/imagemagick: upgrade to 7.1.0.40
      community/imagemagick: upgrade to 7.1.0.41
      community/imagemagick: upgrade to 7.1.0.43
      community/imagemagick: upgrade to 7.1.0.44
      community/imagemagick: upgrade to 7.1.0.45
      community/imagemagick: upgrade to 7.1.0.46
      community/imagemagick: upgrade to 7.1.0.47
      community/imagemagick: upgrade to 7.1.0.48
      community/imagemagick: upgrade to 7.1.0.49
      main/postfix: upgrade to 3.7.3
      main/openssl3: security upgrade to 3.0.6
      main/openssl3: downgrade to 3.0.5
      community/netatalk: upgrade to 3.1.13
      main/libxml2: mitigate cves
      main/bcache-tools: add -dbg
      community/poco: use system deps, place them in -dev
      community/py3-django: fix checksums
      main/darkhttpd: upgrade to 1.14
      main/speex: upgrade to 1.2.1
      community/thunderbird: upgrade to 91.13.1
      main/protobuf: rebuild
      main/expat: upgrade to 2.5.0
      main/curl: security fixes
      community/libtorrent-rasterbar: upgrade to 2.0.8
      community/zsnes: try fix build
      community/zsnes: try fix build pt2
      community/zsnes: try fix build pt3
      community/zsnes: fix c++ target name
      main/py3-mako: upgrade to 1.2.1
      community/py3-waitress: upgrade to 2.1.2
      community/py3-waitress: fix provides
      main/tzdata: upgrade to 2022d
      main/tzdata: upgrade to 2022e
      main/tzdata: upgrade to 2022f
      main/perl-datetime-timezone: upgrade to 2.55
      main/perl-datetime-timezone: upgrade to 2.56
      community/sudo: upgrade to 1.9.11_p3
      community/sudo: upgrade to 1.9.12
      main/musl: backport relr patches
      main/pixman: fix CVE-2022-44638
      main/pixman: bump pkgrel
      community/slony1: fix install dir
      main/cyrus-sasl: enable httpform
      community/py3-importlib-metadata: fix version
      main/tzdata: fix zdump
      community/libvirt: fix crash
      main/graphviz: add gd loader
      main/python3: upgrade to 3.10.8
      community/python3-tkinter: upgrade to 3.10.8
      community/xterm: upgrade to 375

ptrcnull (14):
      community/spot: clean up alsa backend references
      main/libnftnl: upgrade to 1.2.3
      community/ruby-net-ldap: upgrade to 0.17.1
      community/yash: upgrade to 2.53
      community/py3-frozendict: upgrade to 2.3.4
      community/stunnel: upgrade to 5.66
      main/py3-mako: upgrade to 1.2.2
      main/py3-mako: upgrade to 1.2.3
      main/perl-datetime-timezone: upgrade to 2.53
      main/perl-datetime-timezone: upgrade to 2.54
      main/py3-tz: upgrade to 2022.4
      community/suricata: upgrade to 6.0.6
      community/suricata: update url
      community/libhtp: upgrade to 0.5.41

wener (2):
      community/grpc-java: enable aarch64
      community/k3s: upgrade to 1.23.12.1

</pre>
