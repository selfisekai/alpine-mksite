---
title: 'Alpine 3.13.4 released'
date: 2021-03-31
---

Alpine Linux 3.13.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.13.4 of its Alpine Linux operating system.

This release includes a fix for busybox [CVE-2021-28831](https://gitlab.alpinelinux.org/alpine/aports/-/issues/12566)
and kernel fixes for Ampere Mt Jade machines.

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.13.4).

Git Shortlog
------------

<pre>
Andy Postnikov (4):
      community/php7-pecl-redis: upgrade to 5.3.4
      community/php8-pecl-redis: upgrade to 5.3.4
      community/php8-pecl-event: upgrade to 3.0.3
      community/php7-pecl-event: upgrade to 3.0.3

Bart Ribbers (2):
      community/powerdevil: fix dbus usage
      community/libquotient: upgrade to 0.6.6

Francesco Colista (1):
      community/py3-lxml: securit upgrade to fix CVE-2021-28957

J0WI (2):
      main/ldb: upgrade to 2.2.1
      main/samba: security upgrade to 4.13.7

Jakub Jirutka (1):
      main/knot: upgrade to 3.0.5

Kevin Daudt (1):
      main/busybox: fix post-upgrade stat error

Leo (2):
      main/squid: security upgrade to 5.0.5
      community/synapse: security upgrade to 1.30.1

Natanael Copa (18):
      main/linux-lts: fixes for ampere mt jade
      community/jool-modules-lts: rebuild against kernel 5.10.26-r1
      community/rtl8821ce-lts: rebuild against kernel 5.10.26-r1
      community/rtpengine-lts: rebuild against kernel 5.10.26-r1
      main/dahdi-linux-lts: rebuild against kernel 5.10.26-r1
      main/xtables-addons-lts: rebuild against kernel 5.10.26-r1
      main/zfs-lts: rebuild against kernel 5.10.26-r1
      main/linux-lts: upgrade to 5.10.27
      community/jool-modules-lts: rebuild against kernel 5.10.27-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.27-r0
      community/rtpengine-lts: rebuild against kernel 5.10.27-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.27-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.27-r0
      main/zfs-lts: rebuild against kernel 5.10.27-r0
      main/linux-rpi: upgrade to 5.10.27
      community/jool-modules-rpi: rebuild against kernel 5.10.27-r0
      main/zfs-rpi: rebuild against kernel 5.10.27-r0
      ===== release 3.13.4 =====

Oliver Smith (1):
      community/pmbootstrap: upgrade to 1.30.0

Rasmus Thomsen (2):
      community/networkmanager: upgrade to 1.26.6
      community/networkmanager-elogind: upgrade to 1.26.6

Sören Tempel (1):
      main/busybox: security fix CVE-2021-28831

Timo Teräs (1):
      main/apk-tools: upgrade to 2.12.4

omni (1):
      community/webkit2gtk: upgrade to 2.30.6

</pre>
