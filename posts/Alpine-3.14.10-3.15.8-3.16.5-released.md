---
title: 'Alpine 3.14.10, 3.15.8 and 3.16.5 released'
date: 2023-03-29
---

Alpine 3.14.10, 3.15.8 and 3.16.5 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.14.10](https://git.alpinelinux.org/aports/log/?h=v3.14.10)
- [3.15.8](https://git.alpinelinux.org/aports/log/?h=v3.15.8)
- [3.16.5](https://git.alpinelinux.org/aports/log/?h=v3.16.5)

Those releases include security fixes for openssl:

- [CVE-2023-0464](https://security.alpinelinux.org/vuln/CVE-2023-0464)
- [CVE-2023-0465](https://security.alpinelinux.org/vuln/CVE-2023-0465)

