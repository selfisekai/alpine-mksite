---
title: 'Alpine 3.15.2 released'
date: 2022-03-23
---

Alpine Linux 3.15.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.15.2 of its Alpine Linux operating system.

This release includes a fix for libretls [CVE-2022-0778](https://gitlab.alpinelinux.org/alpine/aports/-/commit/91c7a9f3aa296b6d462c5634e7658ebdbff65bb9).

The full lists of changes can be found in the [git log](http://git.alpinelinux.org/aports/log/?h=v3.15.2).

Git Shortlog
------------

<pre>
Dermot Bradley (1):
      community/nomad: upgrade to 1.1.12

Holger Jaekel (1):
      community/gdal: upgrade to 3.4.2

J0WI (2):
      main/libretls: patch CVE-2022-0778
      main/tzdata: upgrade to 2022a

Milan P. Stanić (3):
      community/linux-edge: upgrade to 5.16.15
      community/linux-edge: upgrade to 5.16.16
      community/xorg-server: upgrade to 21.1.3

Natanael Copa (11):
      main/linux-rpi: upgrade to 5.15.30
      community/jool-modules-rpi: rebuild against kernel 5.15.30-r0
      main/zfs-rpi: rebuild against kernel 5.15.30-r0
      main/linux-lts: upgrade to 5.15.30
      community/jool-modules-lts: rebuild against kernel 5.15.30-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.30-r0
      community/rtpengine-lts: rebuild against kernel 5.15.30-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.30-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.30-r0
      main/zfs-lts: rebuild against kernel 5.15.30-r0
      ===== release 3.15.2 =====

Simon Frankenberger (1):
      community/java-common: fix links to jdk executables

psykose (5):
      main/bind: security upgrade to 9.16.27
      main/mariadb: upgrade to 10.6.7
      community/nodejs-current: upgrade to 17.7.2
      community/fwupd: remove bash requirement
      community/nodejs-current: upgrade to 17.8.0
</pre>
