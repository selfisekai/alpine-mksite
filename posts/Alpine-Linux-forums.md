---
title: 'Alpine Linux forums'
date: 2013-06-20
---

# Alpine Linux forums

**Note in 2018**: we have retired the forum. Please see [here](/community/) for latest information about community.

We are recently receiving more and more support questions from users, but our infrastructure does not provide any place for them.
To provide an easy way for users to contact each other and Alpine Linux developers, we have setup Alpine Linux forum.

Register an account on our alpinelinux.org website ([here](http://alpinelinux.org/user/register)) and visit our [forum](/forum).

For development related questions, you are still advised to use our [development mailing list](http://lists.alpinelinux.org/).

Happy posting!
