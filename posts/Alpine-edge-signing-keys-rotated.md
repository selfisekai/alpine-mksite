---
title: 'Alpine Linux edge signing keys rotated'
date: 2022-07-14
---

Alpine Linux edge signing keys rotated
=======================================

Since Alpine Linux 3.15, [new 4096-bits RSA signing keys were
introduced][315-release]. But for the edge branch, these keys were not used yet
to give everyone time to obtain the new public keys.

Now, the edge keys have been rotated as well, meaning new packages will start
getting signed with the new keys.

In case you upgraded to edge and don't have the new keys for some reason,
execute the following command:

    apk add -X https://dl-cdn.alpinelinux.org/alpine/v3.16/main -u alpine-keys

[315-release]:https://alpinelinux.org/posts/Alpine-3.15.0-released.html#upgrade_notes
