---
title: 'Alpine 3.15.1 released'
date: 2022-03-16
---

Alpine Linux 3.15.1 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.15.1 of its Alpine Linux operating system.

This release includes a fix for openssl [CVE-2022-0778](https://security.alpinelinux.org/vuln/CVE-2022-0778).

The full lists of changes can be found in the [git log](http://git.alpinelinux.org/aports/log/?h=v3.15.1).

Git Shortlog
------------

<pre>
6543 (1):
      community/gitea: add patch of a security fix for lfs

Alex Dowad (1):
      main/lua-luaxml: Fix bug on Lua 5.2+

Alex Xu (Hello71) (4):
      community/nss: patch for CVE-2021-43527
      community/nss: upgrade to 3.72.1
      community/qt5-qtwebengine: stack size v2 (fixes #13333)
      community/wine: upgrade to 6.23, enable mingw

Andrew Harris (1):
      main/gmp: patch CVE-2021-43618

Andy Postnikov (31):
      community/php8-pecl-ast: upgrade to 1.0.16
      community/php7-pecl-ast: upgrade to 1.0.16
      community/php8-pecl-rdkafka: upgrade to 5.0.2
      community/php7-pecl-rdkafka: upgrade to 5.0.2
      community/php8-pecl-amqp: upgrade to 1.11.0
      community/php7-pecl-amqp: upgrade to 1.11.0
      community/php8-pecl-xdebug: upgrade to 3.1.2
      community/php7-pecl-xdebug: upgrade to 3.1.2
      main/nodejs: upgrade to 16.13.1
      community/nodejs-current: upgrade to 17.2.0
      community/unit: upgrade to 1.26.1
      community/php8-pecl-couchbase: upgrade to 3.2.2
      community/php7-pecl-couchbase: upgrade to 3.2.2
      community/libcouchbase: upgrade to 3.2.4
      community/php7: upgrade to 7.4.27
      community/php8: upgrade to 8.0.14
      community/tidyhtml: move symlink to libs
      main/nodejs: security upgrade to 16.13.2
      community/nodejs-current: security upgrade to 17.3.1
      community/php7-pecl-redis: upgrade to 5.3.6
      community/php8-pecl-redis: upgrade to 5.3.6
      community/drupal7: security upgrade to 7.86
      community/drupal7: upgrade to 7.87
      community/phpmyadmin: security upgrade to 5.1.2
      community/php7-pecl-xdebug: upgrade to 3.1.3
      community/php8-pecl-xdebug: upgrade to 3.1.3
      community/php7: security upgrade to 7.4.28 - CVE-2021-21708
      community/php8: security upgrade to 8.0.16 - CVE-2021-21708
      community/phpmyadmin: upgrade to 5.1.3
      community/php7-pecl-mailparse: upgrade to 3.1.3
      community/php8-pecl-mailparse: upgrade to 3.1.3

Anjandev Momi (2):
      community/prosody: Security upgrade to 0.11.12
      community/prosody: upgrade to 0.11.13

Ariadne Conill (1):
      main/openssl: provide openssl1.1-compat-libs-static

Arnavion (1):
      community/foot: add -dbg subpackage and mark some others as noarch

Bart Ribbers (21):
      community/pipewire: add missing dep on readline
      community/plasma: upgrade to 5.23.4
      community/qcoro: new aport
      community/plasma-mobile*: upgrade to 21.12
      commuity/fluidsynth: upgrade to 2.2.4
      community/latte-dock: upgrade to 0.10.4
      community/libglibutil: upgrade to 1.0.61
      community/libgbinder: upgrade to 1.1.14
      community/olm: upgrade to 3.2.7
      community/poco: upgrade to 1.11.1
      community/kasts: add missing gst-plugin-good dependency
      community/olm: fix python package
      community/polkit: fix upgrading from polkit-elogind < 0.118
      community/plasmatube: upgrade to 21.12
      community/plasma: upgrade to 5.23.5
      community/qt5-qtwebglplugin: make -dev depend on the main package
      community/pure-maps: upgrade to 2.9.0
      community/knewstuff: backport upstream requested patch
      community/plasma-desktop: backport upstream requested patch
      community/discover: backport upstream requested patch
      community/plasma-settings: fix disabling suspend

Carlo Landmeter (1):
      main/alpine-conf: add upstream fixes

Clayton Craft (1):
      community/mmsd-tng: upgrade to 1.9

Dmitry Zakharchenko (1):
      community/foot: upgrade to 1.10.2

Dominique Martinet (1):
      main/distcc: fix segfault on aarch64

Duncan Bellamy (3):
      main/snappy: backport rtti.patch from edge
      community/ceph: upgrade to 16.2.7
      community/py3-dnsrobocert: upgrade tp 3.15

Francesco Colista (10):
      community/py3-lxml: upgrade to 4.6.5
      community/coredns: backport from edge/community
      community/coredns: fix for plugin and log dirs
      community/coredns: bump pkgrel:
      community/lua-resty-openidc: upgrade to 1.7.5
      community/coredns: fixed init for a correct file logging
      community/jenkins: security upgrade to 2.319.2
      community/jenkins: security upgrade to 2.319.2
      community/bareos: backport fix for php8 compatibility
      community/lua-resty-openidc: added support for OAuth 2.0 Form Post Response Mode

Galen Abell (1):
      community/riot-web: security upgrade to 1.9.7

Guillaume Quintard (1):
      main/varnish: upgrade to 7.0.2

Henrik Riomar (2):
      community/vault: upgrade to 1.9.1
      main/etckeeper: upgrade to 1.18.17

Holger Jaekel (2):
      community/gdal: upgrade to 3.4.1
      community/geos: upgrade to 3.10.2

Humm (1):
      community/sysstat: disable compression of man pages

J0WI (15):
      community/firefox-esr: security upgrade to 91.4.0
      community/tor: upgrade to 0.4.6.8
      main/privoxy: security upgrade to 3.0.33
      main/apache2: security upgrade to 2.4.52
      community/tor: upgrade to 0.4.6.9
      main/vim: security upgrade to 8.2.4173
      community/firefox-esr: security upgrade to 91.6.0
      main/postgresql14: upgrade to 14.2
      main/postgresql13: upgrade to 13.6
      community/postgresql12: upgrade to 12.10
      main/fail2ban: patch CVE-2021-32749
      main/apache2: security upgrade to 2.4.53
      community/libressl: security upgrade to 3.4.3
      main/openssl: security upgrade to 1.1.1n
      main/openssl3: security upgrade to 3.0.2

Jake Buchholz Göktürk (4):
      community/containerd: security update to 1.5.9
      main/tiny-cloud: new aport
      main/yx: new aport
      community/containerd: security update to 1.5.10

Jakob Hauser (1):
      community/modemmanager: add patch "modem-qmi-facility-locks"

Jakub Jirutka (36):
      main/alpine-make-rootfs: upgrade to 0.6.0
      community/alpine-make-vm-image: upgrade to 0.8.0
      community/ruby-net-ldap: upgrade to 0.17.0
      */postgresql1[2-4]: add provides=postgresql-libs to libecpg
      main/ruby: check if "bundled gems" are available in correct versions
      main/ruby-rbs: upgrade to 1.4.0
      main/ruby-typeprof: upgrade to 0.15.2
      community/borgbackup: disable on ppc64le due to py3-pyzmq
      community/imagemagick6: upgrade to 6.9.12.32
      main/clang: remove erroneous patch for Linux::isPIEDefault()
      community/nnn: fix nnn-getplugs symlink
      community/kea: upgrade to 2.0.1
      community/kea-hook-runscript: rebuild against kea 2.0.1
      community/kea-hook-userchk-ldap: rebuild against kea 2.0.1
      main/postgresql1[3-4]: replace poor JIT patch with better one
      main/postgresql14: fix JIT on x86 and make s390x workaround conditional
      community/postgresql12: replace poor JIT patch with better one
      main/pspg: upgrade to 5.5.3
      community/pam-rundir: fix broken mkdir
      community/connman: adjust DBus rules
      community/connman: ensure that tun module is loaded for openvpn
      main/knot: upgrade to 3.1.5
      main/openrc: remove unnecessary kmod from depends
      community/knot-resolver: upgrade to 5.4.4
      main/haveged: upgrade to 1.9.17
      community/ruby-ffi: upgrade to 1.15.5
      community/ruby-listen: upgrade to 3.7.1
      community/libu2f-host: create plugdev group (needed for udev rules)
      community/gtk-layer-shell: disable annoying warning for newer GTK versions
      community/flatpak: security upgrade to 1.12.3
      community/flatpak: add post-install message about XDG directories
      main/busybox: patch modprobe to fix confusing message when loading gzipped module
      main/knot: upgrade to 3.1.6
      community/zsh-syntax-highlighting: fix .revision-hash
      community/wl-clipboard: modernize aport
      community/wl-clipboard: backport bug fixes from upstream master

Kaarle Ritvanen (1):
      main/awall: upgrade to 1.10.2

Kevin Daudt (13):
      main/busybox: fix secfix version
      community/zabbix: upgrade to 5.4.8
      community/zabbix: security upgrade to 5.4.9
      community/zabbix: run agent2 as zabbix user
      community/zabbix: update secfixes
      main/ca-certificates: upgrade to 20211220
      community/polkit: mitigate CVE-2021-4034
      main/py3-pillow: mitigate CVE-2022-22815, CVE-2022-22816
      main/py3-pillow: mitigate CVE-2022-22817
      main/xen: fix secfixes
      main/tcpdump: fix secfixes
      community/git-lfs: fix secfixes
      community/git-lfs2: fix secfixes

Laurent Bercot (1):
      community/tinyssh: remove dependency to ucspi-tcp6

Leo (16):
      community/gpaste: upgrade to 3.42.2
      community/webkit2gtk: upgrade to 2.34.2
      main/mesa: upgrade to 21.2.6
      main/git: upgrade to 2.34.1
      community/tree-sitter: upgrade to 0.20.1
      main/libdrm: upgrade to 2.4.109
      community/gnome-maps: upgrade to 41.1
      community/gnome-chess: upgrade to 41.1
      community/py3-fonttools: upgrade to 4.28.2
      community/gnome-backgrounds: upgrade to 41.0
      community/gnome-clocks: upgrade to 41.0
      community/gnome-initial-setup: upgrade to 41.2
      community/go: upgrade to 1.17.4
      community/vips: upgrade to 8.12.1
      community/py3-treq: security upgrade to 22.1.0
      community/gjs: upgrade to 1.70.1

Leon Marz (1):
      community/openexr: security upgrade to 3.1.4

Leonardo Arena (9):
      community/nextcloud: upgrade to 22.2.3
      community/nextcloud21: upgrade to 21.0.7
      main/bacula: re-enable
      Revert "main/logrotate: rotate /var/log/messages by default"
      main/rsyslog: rotate also messages
      community/py3-fakeredis: upgrade to 1.7.0
      community/nextcloud: upgrade to 22.2.5
      community/nextcloud21: upgrade to 21.0.9
      main/amavis: runtime fixes

Maarten van Gompel (6):
      community/sxmo-wlroots: bugfix backport
      testing/wayout: upgrade to 0.1.1
      community/sxmo-utils: bugfix patch
      testing/wayout: upgrade to 0.1.2
      community/wvkbd: upgrade to 0.5
      community/sxmo-utils: upgrade to 1.6.1

Marian Buschsieweke (1):
      main/intel-ucode: upgrade to 20220207

Mathieu Mirmont (1):
      main/zfs-lts: upgrade to 2.1.2

Michał Polański (6):
      community/skopeo: upgrade to 1.5.2
      community/podman: upgrade to 3.4.4
      community/uriparser: security upgrade to 0.9.6
      community/buildah: upgrade to 1.23.2
      main/flac: security upgrade to 1.3.4
      community/seatd: security upgrade to 0.6.4

Michel Piquemal (1):
      [3.15] main/bluez: upgrade to 5.63

Milan P. Stanić (33):
      community/linux-edge: upgrade to 5.15.5
      main/haproxy: upgrade to 2.4.9
      community/linux-edge: upgrade to 5.15.6
      community/linux-edge: upgrade to 5.15.7
      main/dovecot: upgrade to bugfix 2.3.17.1 release
      community/linux-edge: upgrade to 5.15.8
      community/xorg-server: security upgrade to 21.1.2
      community/linux-edge: upgrade to 5.15.9
      community/linux-edge: upgrade to 5.15.10
      community/linux-edge: upgrade to 5.15.11
      main/haproxy: upgrade to 2.4.10
      community/mutt: bugfix upgrade to 2.1.5
      community/linux-edge: upgrade to 5.15.12
      community/linux-edge: upgrade to 5.15.13
      main/haproxy: upgrade to 2.4.12
      main/postfix: bugfix upgrade to 3.6.4
      main/util-linux: security upgrade to 2.37.3
      community/linux-edge: upgrade to 5.16.1
      community/linux-edge: upgrade to 5.16.3
      community/linux-edge: upgrade to 5.16.4
      community/linux-edge: upgrade to 5.16.5
      community/linux-edge: upgrade to 5.16.7
      community/linux-edge: upgrade to 5.16.8
      main/util-linux: security upgrade to 2.37.4
      community/linux-edge: upgrade to 5.16.10
      main/haproxy: upgrade to 2.4.13
      community/linux-edge: upgrade to 5.16.11
      main/haproxy: bugfix upgrade to 2.4.14
      community/linux-edge: upgrade to 5.16.12
      community/linux-edge: upgrade to 5.16.14
      community/linux-edge: enable CGROUP_BPF where missing
      main/hostapd: upgrade to 2.10 (CVE-2022-23303, CVE-2022-23304)
      main/haproxy: upgrade to 2.4.15

Natanael Copa (79):
      main/python3: add fallback for -gnu soabi
      Revert "main/vim: split gvim to separate package"
      main/linux-lts: enable 9P on all -virt
      main/linux-lts: upgrade to 5.15.5
      community/jool-modules-lts: rebuild against kernel 5.15.5-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.5-r0
      community/rtpengine-lts: rebuild against kernel 5.15.5-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.5-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.5-r0
      main/zfs-lts: rebuild against kernel 5.15.5-r0
      main/libarchive: add secfixes comment
      main/libarchive: CVE-2021-36976 is unfixed
      main/ncurses: add secfixes comment for CVE-2021-39537
      main/lz4: backport fix for CVE-2021-3520
      community/py3-lxml: add secfixes info for CVE-2021-43818
      main/linux-lts: upgrade to 5.15.11
      community/jool-modules-lts: rebuild against kernel 5.15.11-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.11-r0
      community/rtpengine-lts: rebuild against kernel 5.15.11-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.11-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.11-r0
      main/zfs-lts: rebuild against kernel 5.15.11-r0
      community/qemu: bundle tcg driver with system i386/x86_64
      main/linux-lts: enable HID Mayflash
      main/linux-lts: upgrade to 5.15.12
      community/jool-modules-lts: rebuild against kernel 5.15.12-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.12-r0
      community/rtpengine-lts: rebuild against kernel 5.15.12-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.12-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.12-r0
      main/zfs-lts: rebuild against kernel 5.15.12-r0
      main/haserl: enable lua 5.4
      main/linux-lts: upgrade to 5.15.15
      community/jool-modules-lts: rebuild against kernel 5.15.15-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.15-r0
      community/rtpengine-lts: rebuild against kernel 5.15.15-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.15-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.15-r0
      main/zfs-lts: rebuild against kernel 5.15.15-r0
      main/linux-lts: upgrade to 5.15.16
      community/jool-modules-lts: rebuild against kernel 5.15.16-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.16-r0
      community/rtpengine-lts: rebuild against kernel 5.15.16-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.16-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.16-r0
      main/zfs-lts: rebuild against kernel 5.15.16-r0
      main/libcap-ng: enable -static
      main/libarchive: security upgrade to 3.6.0 (CVE-2021-36976)
      main/linux-lts: upgrade to 5.15.26
      community/jool-modules-lts: rebuild against kernel 5.15.26-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.26-r0
      community/rtpengine-lts: rebuild against kernel 5.15.26-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.26-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.26-r0
      main/zfs-lts: rebuild against kernel 5.15.26-r0
      main/git: CVE-2021-46101 does not affect us
      main/linux-lts: upgrade to 5.15.28
      community/jool-modules-lts: rebuild against kernel 5.15.28-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.28-r0
      community/rtpengine-lts: rebuild against kernel 5.15.28-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.28-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.28-r0
      main/zfs-lts: rebuild against kernel 5.15.28-r0
      main/alpine-conf: upgrade to 3.13.1
      main/linux-rpi: upgrade to 5.15.28
      community/jool-modules-rpi: rebuild against kernel 5.15.28-r0
      main/zfs-rpi: rebuild against kernel 5.15.28-r0
      main/linux-rpi: upgrade to 5.15.29
      community/jool-modules-rpi: rebuild against kernel 5.15.29-r0
      main/zfs-rpi: rebuild against kernel 5.15.29-r0
      main/linux-lts: upgrade to 5.15.29
      community/jool-modules-lts: rebuild against kernel 5.15.29-r0
      community/rtl8821ce-lts: rebuild against kernel 5.15.29-r0
      community/rtpengine-lts: rebuild against kernel 5.15.29-r0
      main/dahdi-linux-lts: rebuild against kernel 5.15.29-r0
      main/xtables-addons-lts: rebuild against kernel 5.15.29-r0
      main/zfs-lts: rebuild against kernel 5.15.29-r0
      main/raspberrypi-bootloader: upgrade to 1.20220308
      ===== release 3.15.1 =====

Newbyte (3):
      community/supertux: fix crash on startup
      community/gnome-calculator: upgrade to 41.1
      community/lollypop: upgrade to 1.4.29

Nulo (1):
      main/ruby: security upgrade to 3.0.3

Oliver Smith (3):
      community/pmbootstrap: upgrade to 1.40.0
      community/pmbootstrap: upgrade to 1.41.0
      community/pmbootstrap: upgrade to 1.42.0

Olliver Schinagl (2):
      main/hostapd: codestyle cleanups
      main/hostapd: use correct BSD-3 license

Pablo Correa Gómez (1):
      main/cups: cherry-pick fix for musl incompatibility

Roshless (1):
      community/borgbackup: add missing dependency

Sean McAvoy (4):
      main/bind: upgrade to 9.16.25 upgrade to same version in 3.13, 3.14
      main/samba: security upgrade to 4.15.5
      [3.15] community/jenkins: security update to 2.319.3 CVE-2022-0538
      main/bash: upgrade to 5.1.16

Simon Frankenberger (2):
      community/openjdk17: upgrade to 17.0.2
      community/openjdk11: upgrade to 11.0.14

Sören Tempel (2):
      main/cryptsetup: security upgrade to 2.4.3
      community/firefox-esr: upgrade to 91.5.1

Wolf (4):
      community/libtorrent-rasterbar: use c++17
      main/libusb-compat: depend on libusb
      community/acme-client: upgrade to 1.3.0
      community/deluge: upgrade to 2.0.5

knuxify (3):
      community/blueman: upgrade to 2.2.4
      community/blueman: re-add libappindicator dependency
      community/font-manager: upgrade to 0.8.8

ktprograms (4):
      community/nextcloud: miscellaneous improvements
      community/nextcloud: fix shellcheck warnings
      community/nextcloud: use php8 binary for occ
      main/vim: upgrade to 8.2.3779

macmpi (1):
      main/raspberrypi-bootloader: upgrade to 1.20220120

norve (1):
      community/pdns: upgrade to 4.5.3

omni (10):
      community/qt5-qtwebengine: security upgrade to 5.15.3_git20211127
      community/grafana: security upgrade to 8.2.7
      community/grafana-frontend: upgrade to 8.2.7
      community/qt5-qtwebengine: chromium security upgrade
      main/mbedtls: security upgrade to 2.16.12
      community/consul: security upgrade to 1.10.6
      community/qt5-qtwebengine: chromium security upgrade
      main/zfs: upgrade to 2.1.2
      community/zram-init: depend on util-linux-misc
      community/qt5-qtwebengine: chromium security upgrade

prspkt (6):
      community/foot: upgrade to 1.10.3
      main/expat: security upgrade to 2.4.3
      main/expat: fix secfixes comment
      main/lighttpd: security upgrade to 1.4.64
      main/libwebp: bugfix upgrade to 1.2.2
      main/zsh: security upgrade to 5.8.1

psykose (25):
      community/units: fix python shebang and dependency
      main/postfix: add missing icu-dev to makedepends
      main/mariadb: use MinSizeRel as CMAKE_BUILD_TYPE
      community/moreutils: fix ts-moreutils manpage name
      community/libtorrent-rasterbar: upgrade to 1.2.15
      community/firefox-esr: security upgrade to 91.5.0
      main/expat: upgrade to 2.4.4
      main/nodejs: upgrade to 16.14.0
      community/nodejs-current: upgrade to 17.5.0
      main/expat: security upgrade to 2.4.5
      community/chromium: fix gtk+3.0 dependency
      community/deluge: enable all arches
      community/libtorrent-rasterbar: enable all arches
      community/nodejs-current: upgrade to 17.6.0
      main/expat: upgrade to 2.4.6
      community/firefox-esr: security upgrade to 91.6.1
      main/expat: upgrade to 2.4.7
      community/firefox-esr: security upgrade to 91.7.0
      community/qemu: upgrade to 6.1.1
      community/nodejs-current: upgrade to 17.7.0
      community/nodejs-current: upgrade to 17.7.1
      main/libxml2: upgrade to 2.9.13
      community/gitea: upgrade to 1.15.11
      community/firefox-esr: upgrade to 91.7.1
      main/libxslt: upgrade to 1.1.35

ptrcnull (1):
      main/py3-pillow: mitigate CVE-2022-22817, CVE-2022-24303

</pre>
