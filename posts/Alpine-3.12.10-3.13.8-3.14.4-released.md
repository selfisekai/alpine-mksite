---
title: 'Alpine 3.12.10, 3.13.8 and 3.14.4 released'
date: 2022-03-17
---

Alpine 3.12.10, 3.13.8 and 3.14.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of the releases: 

- [3.12.10](https://git.alpinelinux.org/aports/log/?h=v3.12.10)
- [3.13.8](https://git.alpinelinux.org/aports/log/?h=v3.13.8)
- [3.14.4](https://git.alpinelinux.org/aports/log/?h=v3.14.4)

Those releases include a security fix for openssl [CVE-2022-0778](https://security.alpinelinux.org/vuln/CVE-2022-0778).


