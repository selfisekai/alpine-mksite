---
title: 'Alpine 3.14.3 released'
date: 2021-11-12
---

Alpine Linux 3.14.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.14.3 of its Alpine Linux operating system.

This release includes various security fixes for busybox:

- [CVE-2021-42374](https://security.alpinelinux.org/vuln/CVE-2021-42374)
- [CVE-2021-42375](https://security.alpinelinux.org/vuln/CVE-2021-42375)
- [CVE-2021-42378](https://security.alpinelinux.org/vuln/CVE-2021-42378)
- [CVE-2021-42379](https://security.alpinelinux.org/vuln/CVE-2021-42379)
- [CVE-2021-42380](https://security.alpinelinux.org/vuln/CVE-2021-42380)
- [CVE-2021-42381](https://security.alpinelinux.org/vuln/CVE-2021-42381)
- [CVE-2021-42382](https://security.alpinelinux.org/vuln/CVE-2021-42382)
- [CVE-2021-42383](https://security.alpinelinux.org/vuln/CVE-2021-42383)
- [CVE-2021-42384](https://security.alpinelinux.org/vuln/CVE-2021-42384)
- [CVE-2021-42385](https://security.alpinelinux.org/vuln/CVE-2021-42385)
- [CVE-2021-42386](https://security.alpinelinux.org/vuln/CVE-2021-42386)

The full lists of changes can be found in the [git
log](https://git.alpinelinux.org/aports/log/?h=v3.14.3).

Git Shortlog
------------

<pre>
Andrei Jiroh Eugenio Halili (2):
      community/github-cli: move from unmaintained
      community/github-cli: upgrade to 2.1.0

Andy Postnikov (21):
      main/nodejs: security upgrade to 14.17.6
      community/php7-pecl-xhprof: upgrade to 2.3.5
      community/php8-pecl-xhprof: upgrade to 2.3.5
      main/apache2: security upgrade to 2.4.49
      community/php8: security upgrade to 8.0.11 (CVE-2021-21706) and improve phar()
      community/php7: security upgrade to 7.4.24 (CVE-2021-21706)
      community/php7-pecl-event: upgrade to 3.0.6
      community/php8-pecl-event: upgrade to 3.0.6
      community/php7-pecl-uploadprogress: upgrade to 1.1.4 and modernize
      community/php8-pecl-uploadprogress: upgrade to 1.1.4 and modernize
      community/composer: security upgrade to 2.1.9 (CVE-2021-41116)
      community/nodejs-current: security upgrade to 16.11.1
      main/nodejs: security upgrade to 14.18.1
      community/php7-pecl-igbinary: upgrade to 3.2.6
      community/php8-pecl-igbinary: upgrade to 3.2.6
      community/php7-pecl-apcu: upgrade to 5.1.21
      community/php8-pecl-apcu: upgrade to 5.1.21
      main/samba: security upgrade to 4.14.8 - CVE-2021-3671
      community/php8: security upgrade to 8.0.12 - CVE-2021-21703
      community/php7: security upgrade to 7.4.25 - CVE-2021-21703
      community/gitea: disable on aarch64 - fail tests

Apachez (1):
      community/ntpsec: support running as non-root user

Ariadne Conill (9):
      main/squashfs-tools: security upgrade to 4.5 (CVE-2021-40153)
      main/xen: add mitigations for XSA-378 through XSA-383
      main/xen: add mitigation for XSA-384 (CVE-2021-28701)
      main/vim: security upgrade to 8.2.3437 (CVE-2021-3770)
      main/curl: security upgrade to 7.79.0 (CVE-2021-22945, CVE-2021-22946, CVE-2021-22947)
      main/botan: add mitigation for CVE-2021-40529
      main/squid: add mitigation for CVE-2021-28116
      main/openssh: add mitigation for CVE-2021-41617
      main/openrc: add mitigation for CVE-2021-42341

Bart Ribbers (1):
      community/qt5-qtwebengine: update removal of glibc check patch

Carlo Landmeter (1):
      main/lxc-templates-legacy: add support for riscv64

Craig Andrews (1):
      community/wine: upgrade to 6.18

Daniel Néri (1):
      main/xen: upgrade to 4.15.1

Dmitriy Kovalkov (1):
      main/squid: patch CVE-2021-41611

Duncan Bellamy (6):
      main/pjproject: security upgrade to 2.11.1 * supercedes !23272
      community/ceph: upgrade to 16.2.6 * https://ceph.io/en/news/blog/2021/v16-2-6-pacific-released
      community/snowball: add libstemmer.so.2 to libstemmer * fixes #12852
      community/vectorscan: build with lowest armv8 arch for aarch64
      community/rspamd: rebuild against vectorscan
      community/ceph: backport 11-s3_expiration_header.patch from edge

Francesco Colista (2):
      community/snowball: fix shared library exports
      community/acme.sh : fixed dependency with socat, upgrade to 3.0.0

Galen Abell (1):
      community/riot-web: security upgrade to 1.8.4

Henrik Riomar (4):
      community/vault: security upgrade to 1.7.4
      main/rdiff-backup: fix version reported as 0.0.0
      community/fio: disable build system cpu optimizations
      community/vault: upgrade to 1.7.6

J0WI (19):
      community/mozjs78: security upgrade to 78.14.0
      community/cpio: patch CVE-2021-3818
      main/gd: security fixes
      main/ghostscript: patch CVE-2021-3781
      community/firefox-esr: security upgrade to 78.13.0
      main/redis: security upgrade to 6.2.6
      main/apache2: security upgrade to 2.4.50
      community/mozjs78: security upgrade to 78.15.0
      main/apache2: security upgrade to 2.4.51
      main/strongswan: patch CVE-2021-41990 and CVE-2021-41991
      main/tzdata: upgrade to 2021e
      community/py3-sqlparse: security upgrade to 0.4.2
      community/exiv2: security upgrade to 0.27.5
      community/py3-impacket: security upgrade to 0.9.23
      main/perl-datetime-timezone: upgrade to 2.51
      main/perl-mozilla-ca: upgrade to 20211001
      community/ffmpeg: security upgrade to 4.4.1
      community/firefox-esr: security upgrade to 78.14.0
      community/firefox-esr: security upgrade to 78.15.0

Jake Buchholz Göktürk (2):
      community/containerd: security update to 1.5.7
      community/docker: security update to 20.10.9

Jakub Jirutka (5):
      community/wofi: fix dependencies, add -dev subpackage
      community/swaylock: fix --version
      community/swaylock: backport two upstream patches
      main/gnupg: upgrade to 2.2.31
      main/gnupg: fix secfix CVE-2020-25125 assigned to wrong pkgver

José Alberto Orejuela García (1):
      community/texlive: fix trigger script

Kaarle Ritvanen (1):
      main/awall: upgrade to 1.9.2

Kevin Daudt (7):
      community/salt: security upgrade to 3003.3
      main/curl: upgrade to 7.79.1
      main/vim: update secfixes
      community/zabbix: upgrade to 5.4.5
      main/apache2: change source to dlcdn.apache.org
      main/alpine-keys: add new 4096-bits builder keys
      community/zabbix: upgrade to 5.4.7

Leo (22):
      community/synapse: security upgrade to 1.41.1
      main/libgcrypt: security upgrade to 1.9.4
      community/osinfo-db: upgrade to 20210809
      community/osinfo-db: upgrade to 20210903
      community/go: security upgrade to 1.16.8
      community/go: disable net/http test due to timeouts on armv7
      community/go: disable http_test
      community/*: rebuild for go1.16.8
      community/libexif: security upgrade to 0.6.23
      community/fetchmail: security upgrade to 6.4.22
      main/nettle: security upgrade 3.7.3
      main/geoip: remove cron
      community/webkit2gtk: security upgrade to 2.32.4
      community/flatpak: security upgrade to 1.10.4
      community/flatpak: security upgrade to 1.10.5
      community/remind: fix path of source=
      community/anthy: fix source=
      main/perl-datetime-timezone: upgrade to 2.48
      main/perl-datetime-timezone: upgrade to 2.49
      community/go: security upgrade to 1.16.10
      community/*: rebuild archive/zip users
      community/gitea: skip time-sensitive test

Leonardo Arena (2):
      community/nextcloud: upgrade to 21.0.5
      community/nextcloud20: upgrade to 20.0.13

Martin Kaesberger (1):
      community/chromium: upgrade to 93.0.4577.82

Michał Polański (1):
      community/caddy: upgrade to 2.4.5

Milan P. Stanić (6):
      main/haproxy: upgrade to 2.4.4
      main/tzdata: upgrade to 2021b
      main/tzdata: upgrade to 2021c
      main/lxc: upgrade to 4.0.10
      main/tzdata: upgrade to 2021d
      main/postfix: bugfix upgrade to 3.6.3

Natanael Copa (20):
      main/numactl: re-enable on armv7 and armhf
      community/qemu: enable numa support
      main/lxc: add riscv64 as a valid personality
      main/util-linux: security upgrade to 2.37.2 (CVE-2021-37600)
      main/squashfs-tools: fix CVE-2021-41072
      main/busybox: fix CVE-2021-42374
      main/busybox: fix CVE-2021-42375
      main/linux-lts: upgrade to 5.10.78
      community/jool-modules-lts: rebuild against kernel 5.10.78-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.78-r0
      community/rtpengine-lts: rebuild against kernel 5.10.78-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.78-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.78-r0
      main/zfs-lts: rebuild against kernel 5.10.78-r0
      main/linux-rpi: upgrade to 5.10.78
      community/jool-modules-rpi: rebuild against kernel 5.10.78-r0
      main/zfs-rpi: rebuild against kernel 5.10.78-r0
      main/busybox: security fixes for awk
      main/raspberrypi-bootloader: upgrade to 1.20211029
      ===== release 3.14.3 =====

Newbyte (1):
      community/sysbench: move from testing

Oliver Smith (4):
      community/pmbootstrap: upgrade to 1.37.0
      community/geary: rebuild for so:libstemmer.so.2
      community/pmbootstrap: upgrade to 1.38.0
      community/pmbootstrap: upgrade to 1.39.0

Thomas Liske (1):
      community/ifstate: upgrade to 1.5.6

kpcyrd (1):
      community/acme-redirect: upgrade to 0.5.3

omni (3):
      community/tor: add CVE numbers for 0.4.5.9-r0
      community/qt5-qtwebengine: security upgrade to 5.15.3_git20211006
      community/qt5-qtwebengine: chromium security upgrade
</pre>
