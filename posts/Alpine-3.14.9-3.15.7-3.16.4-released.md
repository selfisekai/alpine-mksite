---
title: 'Alpine 3.14.9, 3.15.7 and 3.16.4 released'
date: 2023-02-10
---

Alpine 3.14.9, 3.15.7 and 3.16.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.14.9](https://git.alpinelinux.org/aports/log/?h=v3.14.9)
- [3.15.7](https://git.alpinelinux.org/aports/log/?h=v3.15.7)
- [3.16.4](https://git.alpinelinux.org/aports/log/?h=v3.16.4)

Those releases include security fixes for openssl:

- [CVE-2022-4203](https://security.alpinelinux.org/vuln/CVE-2022-4203)
- [CVE-2022-4304](https://security.alpinelinux.org/vuln/CVE-2022-4304)
- [CVE-2022-4450](https://security.alpinelinux.org/vuln/CVE-2022-4450)
- [CVE-2023-0215](https://security.alpinelinux.org/vuln/CVE-2023-0215)
- [CVE-2023-0216](https://security.alpinelinux.org/vuln/CVE-2023-0216)
- [CVE-2023-0217](https://security.alpinelinux.org/vuln/CVE-2023-0217)
- [CVE-2023-0286](https://security.alpinelinux.org/vuln/CVE-2023-0286)
- [CVE-2023-0401](https://security.alpinelinux.org/vuln/CVE-2023-0401)

