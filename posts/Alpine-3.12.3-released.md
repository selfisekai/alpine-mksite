---
title: 'Alpine 3.12.3 released'
date: 2020-12-16
---

Alpine Linux 3.12.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.12.3 of its Alpine Linux operating system.

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.12.3).

Git Shortlog
------------

<pre>
J0WI (3):
      main/openssl: security upgrade to 1.1.1i
      community/imagemagick: security upgrade to 7.0.10.48
      community/imagemagick6: security upgrade to 6.9.11.48

Jakub Jirutka (1):
      main/mbedtls: upgrade to 2.16.9

Laszlo Soos (1):
      community/runit: fix UDP support for svlogd

Leo (13):
      main/nrpe: fix CVE-2020-6581 and CVE-2020-6582
      main/py3-django: fix CVE-2020-24583 and CVE-2020-24584
      main/curl: fix CVE-2020-8285 and CVE-2020-8286
      main/openssl: upgrade to 1.1.1h
      community/php7-pecl-imagick: rebuild
      main/p11-kit: security upgrade to 0.23.22
      main/nrpe: revert fixes for CVE-2020-6581 and CVE-2020-6582
      main/nrpe: rebuild after revert
      community/xscreensaver: upgrade to 5.45
      community/kpmcore: fix CVE-2020-27187
      community/py3-lxml: fix CVE-2020-27783
      community/minidlna: fix CVE-2020-12695 and CVE-2020-28926
      main/openssh: fix CVE-2020-14145

Natanael Copa (13):
      main/linux-lts: upgrade to 5.4.83
      community/jool-modules-lts: rebuild against kernel 5.4.83-r0
      community/rtl8821ce-lts: rebuild against kernel 5.4.83-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.83-r0
      community/wireguard-lts: rebuild against kernel 5.4.83-r0
      main/dahdi-linux-lts: rebuild against kernel 5.4.83-r0
      main/drbd-lts: rebuild against kernel 5.4.83-r0
      main/xtables-addons-lts: rebuild against kernel 5.4.83-r0
      main/zfs-lts: rebuild against kernel 5.4.83-r0
      main/linux-rpi: upgrade to 5.4.83
      community/jool-modules-rpi: rebuild against kernel 5.4.83-r0
      community/wireguard-rpi: rebuild against kernel 5.4.83-r0
      ===== release 3.12.3 =====

Paolo Bonzini (1):
      main/samurai: add query tool

TBK (1):
      community/zbar: rebuild against imagemagick 7.0.10.48

</pre>
