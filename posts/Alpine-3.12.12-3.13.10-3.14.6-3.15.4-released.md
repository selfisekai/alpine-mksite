---
title: 'Alpine 3.12.12, 3.13.10, 3.14.6 and 3.15.4 released'
date: 2022-04-04
---

Alpine 3.12.12, 3.13.10, 3.14.6 and 3.15.4 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases to address busybox
[CVE-2022-28391](https://security.alpinelinux.org/vuln/CVE-2022-28391)

- [3.12.12](https://git.alpinelinux.org/aports/log/?h=v3.12.12)
- [3.13.10](https://git.alpinelinux.org/aports/log/?h=v3.13.10)
- [3.14.6](https://git.alpinelinux.org/aports/log/?h=v3.14.6)
- [3.15.4](https://git.alpinelinux.org/aports/log/?h=v3.15.4)

