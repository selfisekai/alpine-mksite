---
title: 'Alpine 3.12.2 released'
date: 2020-12-10
---

Alpine Linux 3.12.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.12.2 of its Alpine Linux operating system.

The full lists of changes can be found in the [git
log](https://git.alpinelinux.org/cgit/aports/log/?h=v3.12.2).

Git Shortlog
------------

<pre>
6543 (1):
      community/gitea: upgrade to 1.12.6

Alex Denes (1):
      main/postgresql: security upgrade to 12.5

Andy Postnikov (14):
      community/composer: upgrade to 1.10.15
      community/php7-pecl-timezonedb: upgrade to 2020.4
      community/composer: upgrade to 1.10.16
      community/php7: upgrade to 7.3.24
      community/composer: upgrade to 1.10.17
      community/drupal7: security upgrade to 7.74 - CVE-2020-13671
      community/drupal7: security upgrade to 7.75
      community/php7: upgrade to 7.3.25
      community/php7-pecl-xhprof: upgrade to 2.2.3
      community/php7-pecl-yaml: upgrade to 2.2.0
      community/phpldapadmin: security upgrade to 1.2.6.2
      community/drupal7: upgrade to 7.76
      community/drupal7: upgrade to 7.77
      community/composer: upgrade to 1.10.19

Ariadne Conill (2):
      main/musl: security fix for CVE-2020-28928
      main/tzdata: switch to fat format

Bart Ribbers (1):
      community/sddm: fix CVE-2020-28049

Daniel Néri (3):
      main/xen: security fix for XSA-351
      main/xen: security fix for XSA-355
      main/xen: CVE-2020-28368 assigned to XSA-351

Francesco Colista (2):
      main/lxc: upgrade to 4.0.5
      main/libvirt: fix for virsh requires btrfs to create dir based pool

Henrik Riomar (4):
      main/xen: security upgrade to 4.13.2
      community/intel-ucode: security upgrade to 20201110
      community/intel-ucode: security upgrade to 20201112
      main/asterisk: stable upgrade to 16.14.1

Holger Jaekel (1):
      community/gdal: upgrade to 3.1.4

J0WI (2):
      main/samba: security upgrade to 4.12.9
      community/tor: security upgrade to 0.4.3.7

Jake Buchholz (1):
      [3.12] community/containerd: update to 1.3.9

Jakub Jirutka (2):
      community/sloci-image: upgrade to 0.1.1
      community/muacme: upgrade to 0.3.1

Kaarle Ritvanen (2):
      community/zoneminder: add missing dependency
      main/apache2: fix error subpackage

Kevin Daudt (2):
      community/zabbix: add patch for agent2 on 32-bits arches
      community/zabbix: upgrade to 5.0.6

Leo (21):
      main/perl-datetime-timezone: upgrade to 2.41
      main/perl-datetime-timezone: upgrade to 2.42
      main/perl-datetime-timezone: upgrade to 2.43
      community/mupdf: fix CVE-2020-26519
      main/open-iscsi: upgrade to 2.1.2
      main/samba: upgrade to 4.12.8
      main/tmux: add missing secfixes info
      main/tcpdump: fix CVE-2020-8037
      main/krb5: security upgrade to 1.18.3
      community/tor: disable zstd on armhf
      community/opensc: fix CVE-2020-26750, CVE-2020-26571, CVE-2020-26572
      community/wireshark: security upgrade to 3.2.8
      community/xorg-server: security upgrade to 1.20.10
      main/nsd: security upgrade to 4.3.4
      remove patch that shouldn't be here
      community/x11vnc: fix CVE-2020-29074 and build with -fno-common
      main/curl: fix CVE-2020-8231
      community/sane: upgrade to 1.0.31
      main/bluez: fix CVE-2020-27153
      community/wireshark: security upgrade to 3.2.9
      main/openldap: fix a few CVEs

Leonardo Arena (3):
      community/zabbix: rundir is needed for control socket
      community/zabbix: upgrade to 5.0.5
      community/nextcloud: upgrade to 18.0.12

Milan P. Stanić (3):
      main/tmux: upgrade to 3.1c
      main/haproxy: upgrade to 2.1.10
      main/postfix: upgrade to 3.5.8

Natanael Copa (21):
      main/squid: security upgrade to 4.13
      main/kamailio: bugfix upgrade to 5.3.6
      main/kamailio: bugfix upgrade to 5.3.7
      main/kamailio: backport upstream fix
      community/raptor2: backport fix for CVE-2017-18926
      community/raptor2: modernize
      community/libvncserver: add CVE-2020-25708 to secfixes comment
      main/linux-lts: enable brmcfmac on armv7 and update configs
      main/linux-lts: upgrade to 5.4.82
      community/jool-modules-lts: rebuild against kernel 5.4.82-r0
      community/rtl8821ce-lts: rebuild against kernel 5.4.82-r0
      community/virtualbox-guest-modules-lts: rebuild against kernel 5.4.82-r0
      community/wireguard-lts: upgrade to 1.0.20201112 / 5.4.82-r0
      main/dahdi-linux-lts: rebuild against kernel 5.4.82-r0
      main/drbd-lts: rebuild against kernel 5.4.82-r0
      main/xtables-adons-lts: rebuild against kernel 5.4.82-r0
      main/zfs-lts: rebuild against kernel 5.4.82-r0
      main/linux-rpi: upgrade to 5.4.82
      community/jool-modules-rpi: rebuild against kernel 5.4.82-r0
      community/wireguard-rpi: upgrade to 1.0.20201112 / 5.4.82-r0
      ===== release 3.12.2 =====

Rasmus Thomsen (11):
      community/chromium: upgrade to 86.0.4240.111
      community/webkit2gtk: upgrade to 2.30.3
      community/librsvg: upgrade to 2.48.9
      community/simple-scan: upgrade to 3.36.7
      community/gnome-desktop: upgrade to 3.36.8
      community/gnome-2048: upgrade to 3.36.8
      community/gnome-music: upgrade to 3.36.7
      community/firefox-esr: upgrade to 78.5.0
      community/firefox: upgrade to 83.0
      community/cbindgen: upgrade to 0.15.0
      main/nss: upgrade to 3.59

Simon Frankenberger (1):
      community/openjdk11: security upgrade to 11.0.9

Sören Tempel (1):
      community/radare2: security upgrade to 4.5.1

nick (1):
      community/librdkafka: upgrade to 1.4.4

</pre>
