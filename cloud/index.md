<!-- begin cloud -->
<script type="text/javascript" src="/js/cloud.js"></script>
<div class="pure-g">

    <div class="pure-u-1">
        <div class="l-box">
            <h1>Cloud Images</h1>
            <p> Only AWS images are launchable (select a "Launch Region" to reveal region
                links) but images are also downloadable for importing into your own cloud
                accounts.
            </p>
            <p> NoCloud, Azure, GCP, and OCI images are considered <i>beta</i> quality,
                they have been successfully imported, launched, and logged into; however,
                unknown issues may be discovered as more people begin to use them.
            </p>
            <p> Image variants are assembled for cloud-valid combinations of CPU architecture
                (aarch64, x86_64), firmware (BIOS/UEFI), and instance bootstrap systems (<a
                href="https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud">Tiny Cloud</a>
                and <a href="https://cloud-init.io">cloud-init</a>).
            </p>
            <p> The login user for all images is <b><kbd>alpine</kbd></b>. SSH keys for this
                user will be installed from the cloud's instance metadata service (IMDS).
            </p>
            <p> <i class="fa fa-lock"></i>&nbsp;GPG &mdash;
                <a href="/keys/tomalok.asc">F26A DFAD BAE7 02EF 7AF6 3745 9DA7 EF23 BFFC DF22</a>
            </p>
            <p> <i>These images are built and published using the <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images">
                Alpine Linux Cloud Images Builder</a>.  To report problems or request features,
                please open an <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images/-/issues">
                issue</a> with <u>detailed</u> information.</i>
            </p>
            <form class="pure-form pure-form-stacked">
                <fieldset>
                    <div class="pure-g">
                        <div class="pure-u-1 pure-u-md-1-3">
                            <label for="cloud"><b>Cloud Provider</b></label>
                            <select id="f_cloud" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.clouds}}
                                <option value="{{cloud}}">{{cloud_name}}</option>
                                {{/cloud/releases.filters.clouds}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-6">
                            <label for="region"><b>Launch Region</b></label>
                            <select id="f_region" class="filter" onchange="filter(this.id)">
                                <option value="">(select)</option>
                                {{#cloud/releases.filters.regions}}
                                <option value="{{region}}" id="f_{{region}}"
                                    class="f_cloud {{#clouds}}v_{{cloud}}{{/clouds}}">{{region}}</option>
                                {{/cloud/releases.filters.regions}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="release"><b>Release</b></label>
                            <select id="f_release" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.versions}}
                                <option value="{{release}}">{{release}}</option>
                                {{/cloud/releases.versions}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="arch"><b>Arch</b></label>
                            <select id="f_arch" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.archs}}
                                <option value="{{arch}}">{{arch_name}}</option>
                                {{/cloud/releases.filters.archs}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="firmware"><b>Firmware</b></label>
                            <select id="f_firmware" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.firmwares}}
                                <option value="{{firmware}}">{{firmware_name}}</option>
                                {{/cloud/releases.filters.firmwares}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="bootstrap"><b>Bootstrap</b></label>
                            <select id="f_bootstrap" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.bootstraps}}
                                <option value="{{bootstrap}}">{{bootstrap_name}}</option>
                                {{/cloud/releases.filters.bootstraps}}
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
            <br/>
        </div>
    </div>

    {{#cloud/releases.versions}}
    <div id="f_{{release}}" class="pure-u-1 pure-u-md-1-2 f_release v_{{release}}">
        <div class="cloud download">
            <h2 title="End of Life: {{end_of_life}}">{{release}}</h2>
            {{#images}}
            <div id="f_{{variant}}" class="f_arch v_{{arch}} f_firmware v_{{firmware}} f_bootstrap v_{{bootstrap}}">
                <table class="pure-table pure-table-bordered">
                    <thead>
                        <tr><td colspan="3" align="center">
                            <b title="Released: {{released}}"">{{arch}} • {{firmware}} • {{bootstrap}}</b>
                            <table class="pure-table pure-table-horizontal" style="border-top: 1.5px solid #999;">
                                {{#downloads}}
                                <tr id="f_{{variant}}_{{cloud}}" class="f_cloud v_{{cloud}}">
                                    <td align="center"><a href="{{{image_url}}}.{{{image_format}}}" class="green-button"
                                        title="{{image_name}}.{{image_format}}">
                                        <i class="fa fa-download"></i>&nbsp;{{cloud}}
                                    </a></td>
                                    <td align="center">
                                        <a title="GPG signature"
                                           class="pure-button checksums"
                                           href="{{{image_url}}}.asc"
                                           ><i class="fa fa-lock"></i>&nbsp;gpg</a>
                                        <a title="YAML metadata"
                                           class="pure-button checksums"
                                           href="{{{image_url}}}.yaml"
                                           ><i class="fa fa-info-circle"></i>&nbsp;meta</a>
                                        {{#regions}}
                                        <a id="f_{{variant}}_{{cloud}}_{{region}}_l"
                                            class="pure-button launch f_cloud v_{{cloud}} f_region v_{{region}}"
                                            title="launch in {{cloud}} / {{region}}"
                                            target="_blank" rel="noopener noreferrer"
                                            href="{{{launch_url}}}" style="display: none;"
                                            ><i class="fa fa-rocket"></i>&nbsp;launch</a>
                                        <a id="f_{{variant}}_{{cloud}}_{{region}}_i"
                                            class="pure-button launch f_cloud v_{{cloud}} f_region v_{{region}}"
                                            title="image in {{cloud}} / {{region}}"
                                            target="_blank" rel="noopener noreferrer"
                                            href="{{{region_url}}}" style="display: none;"
                                            ><i class="fa fa-info-circle"></i>&nbsp;image</a>
                                        {{/regions}}
                                    </td>
                                </tr>
                                {{/downloads}}
                            </table>
                        </td></tr>
                    </thead>
                </table>
            </div>
            {{/images}}
        </div>
    </div>
    {{/cloud/releases.versions}}

</div>
<!-- end cloud -->
